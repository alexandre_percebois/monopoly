const { app, BrowserWindow, ipcMain } = require('electron')
const path = require('path')
const url = require('url')
const WebSocket = require('ws');

const { CarteChance, CarteCommunaute } = require('./carteChance');
const { Case, Gare, Service, Taxe, Chance, Communaute, Propriete } = require('./propriete');
// Gardez l'objet window dans une constante global, sinon la fenêtre sera fermée
// automatiquement quand l'objet JavaScript sera collecté par le ramasse-miettes.
let win;
let ws;
let partie = {
  joueurs: [],
  park: 0,
  websocketJoueur: [],
  aQuiLeTour: 0,
  Payer: Payer,
  GagnerArgent: GagnerArgent
};

let cartesChances = [];
cartesChances.push(new CarteChance(1, "Rendez-vous avenue Henri Martin.\nSi vous passez par la case Départ\nrecevez 200 €", 24));
cartesChances.push(new CarteChance(1, "Placez vous sur la case Départ.", 0));
cartesChances.push(new CarteChance(1, "Rendez-vous rue de la Paix.", 39));
cartesChances.push(new CarteChance(1, "Avancez boulevard de la Villette.\nSi vous passez par la case Départ\nrecevez 200 €", 11));
cartesChances.push(new CarteChance(1, "Allez à la gare de Lyon.\nSi vous passez par la case Départ\nrecevez 200 €", 15));
cartesChances.push(new CarteChance(2, "Reculez de trois cases.", 3));
cartesChances.push(new CarteChance(3, "Votre immeuble et votre prêt rapportent.\nVous devez toucher 150 €", 150));
cartesChances.push(new CarteChance(3, "La banque vous verse un dividende de 50 €", 50));
cartesChances.push(new CarteChance(3, "Vous avez gagné le prix de mots croisés.\nRecevez 100 €", 100));
cartesChances.push(new CarteChance(4, "Amende pour ivresse :\n20 €", 20));
cartesChances.push(new CarteChance(4, "Amende pour excès de vitesse :\n15 €", 15));
cartesChances.push(new CarteChance(4, "Payez pour frais de scolarité\n150 €", 150));
cartesChances.push(new CarteChance(5, "Vous êtes imposé pour les réparations de voirie à raison de :\n40 € par maison,\n115 € par hôtel", 40, 115));
cartesChances.push(new CarteChance(5, "Faites des réparations dans toutes vos maisons.\nVersez pour chaque maison 25 €\nVersez pour chaque hôtel 100 €", 25, 100));
cartesChances.push(new CarteChance(6, "Allez en prison.\nAvancez tout droit en prison.\nNe passez pas par la case Départ.\nNe recevez pas 200 €"));
cartesChances.sort(() => {
  return .5 - Math.random();
});

let cartesCommunautes = [];
cartesCommunautes.push(new CarteCommunaute(1, "Payez votre Police d'Assurance s'élevant à 50 €", 50));
cartesCommunautes.push(new CarteCommunaute(1, "Payez la note du médecin 50 €", 50));
cartesCommunautes.push(new CarteCommunaute(1, "Payez à l'Hôpital 100 €", 100));
cartesCommunautes.push(new CarteCommunaute(2, "Les Contributions vous remboursent la somme de 20 €", 20));
cartesCommunautes.push(new CarteCommunaute(2, "La vente de votre stock vous rapporte 50 €", 50));
cartesCommunautes.push(new CarteCommunaute(2, "Erreur de la banque en votre faveur.\nRecevez 200 €", 200));
cartesCommunautes.push(new CarteCommunaute(2, "Vous avez gagné le deuxième prix de beauté.\nRecevez 10 €", 10));
cartesCommunautes.push(new CarteCommunaute(2, "Recevez votre intérêt sur l'emprunt à 7%.\n25€", 25));
cartesCommunautes.push(new CarteCommunaute(2, "Recevez votre revenu annuel\n100€", 100));
cartesCommunautes.push(new CarteCommunaute(2, "Vous héritez de 100 €", 100));
cartesCommunautes.push(new CarteCommunaute(3, "Allez en prison.\nAvancez tout droit en prison.\nNe passez pas par la case Départ.\nNe recevez pas 200 €"));
cartesCommunautes.push(new CarteCommunaute(4, "Placez vous sur la case Départ.", 0));
cartesCommunautes.push(new CarteCommunaute(5, "Retournez à Belleville.", 1));
cartesCommunautes.push(new CarteCommunaute(6, "C'est votre anniversaire :\nchaque joueur doit vous donner 10 €", 10));
cartesCommunautes.sort(() => {
  return .5 - Math.random();
});

let cases = [];
cases.push({ appartient: -1, element: new Case(1) });
cases.push({ appartient: -1, element: new Propriete(1, 1, "Boulevard de Belleville", 60, 2, 10, 30, 90, 160, 250, 50) });
cases.push({ appartient: -1, element: new Communaute(cartesCommunautes) });
cases.push({ appartient: -1, element: new Propriete(3, 1, "Rue Lecourbe", 60, 4, 20, 60, 180, 320, 450, 50) });
cases.push({ appartient: -1, element: new Taxe(200) });
cases.push({ appartient: -1, element: new Gare(0, "Gare Montparnasse") });
cases.push({ appartient: -1, element: new Propriete(1, 2, "Rue de Vaugirard", 100, 6, 30, 90, 270, 400, 550, 50) });
cases.push({ appartient: -1, element: new Chance(cartesChances) });
cases.push({ appartient: -1, element: new Propriete(2, 2, "Rue de Courcelles", 100, 6, 30, 90, 270, 400, 550, 50) });
cases.push({ appartient: -1, element: new Propriete(3, 2, "Avenue de la République", 120, 8, 40, 100, 300, 450, 600, 50) });
cases.push({ appartient: -1, element: new Case(2) });

cases.push({ appartient: -1, element: new Propriete(1, 3, "Boulevard de la Villette", 140, 10, 50, 150, 450, 625, 750, 100) });
cases.push({ appartient: -1, element: new Service(1, "Compagnie de distribution d'électricité") });
cases.push({ appartient: -1, element: new Propriete(2, 3, "Avenue de Neuilly", 140, 10, 50, 150, 450, 625, 750, 100) });
cases.push({ appartient: -1, element: new Propriete(3, 3, "Rue de Paradis", 160, 12, 60, 180, 500, 700, 900, 100) });
cases.push({ appartient: -1, element: new Gare(1, "Gare de Lyon") });
cases.push({ appartient: -1, element: new Propriete(1, 4, "Avenue Mozart", 180, 14, 70, 200, 550, 750, 950, 100) });
cases.push({ appartient: -1, element: new Communaute(cartesCommunautes) });
cases.push({ appartient: -1, element: new Propriete(2, 4, "Boulevard Saint-Michel", 180, 14, 70, 200, 550, 750, 950, 100) });
cases.push({ appartient: -1, element: new Propriete(3, 4, "Place Pigalle", 200, 16, 80, 220, 600, 800, 1000, 100) });
cases.push({ appartient: -1, element: new Case(3) });

cases.push({ appartient: -1, element: new Propriete(1, 5, "Avenue Matignon", 220, 18, 90, 250, 700, 875, 1050, 150) });
cases.push({ appartient: -1, element: new Chance(cartesChances) });
cases.push({ appartient: -1, element: new Propriete(2, 5, "Boulevard Malesherbes", 220, 18, 90, 250, 700, 875, 1050, 150) });
cases.push({ appartient: -1, element: new Propriete(3, 5, "Avenue Henri-Martin", 240, 20, 100, 300, 750, 925, 1100, 150) });
cases.push({ appartient: -1, element: new Gare(2, "Gare du Nord") });
cases.push({ appartient: -1, element: new Propriete(1, 6, "Faubourg Saint-Honoré", 260, 22, 110, 330, 800, 975, 1150, 150) });
cases.push({ appartient: -1, element: new Propriete(2, 6, "Place de la Bourse", 260, 22, 110, 330, 800, 975, 1150, 150) });
cases.push({ appartient: -1, element: new Service(2, "Compagnie de distribution des eaux") });
cases.push({ appartient: -1, element: new Propriete(3, 6, "Rue la Fayette", 280, 24, 120, 360, 850, 1125, 1200, 150) });
cases.push({ appartient: -1, element: new Case(4) });

cases.push({ appartient: -1, element: new Propriete(1, 7, "Avenue de Breteuil", 300, 26, 130, 390, 900, 1100, 1275, 200) });
cases.push({ appartient: -1, element: new Propriete(2, 7, "Avenue Foch", 300, 26, 130, 390, 900, 1100, 1275, 200) });
cases.push({ appartient: -1, element: new Communaute(cartesCommunautes) });
cases.push({ appartient: -1, element: new Propriete(3, 7, "Boulevard des Capucines", 320, 28, 150, 450, 1000, 1200, 1400, 200) });
cases.push({ appartient: -1, element: new Gare(3, "Gare Saint-Lazare") });
cases.push({ appartient: -1, element: new Chance(cartesChances) });
cases.push({ appartient: -1, element: new Propriete(1, 8, "Avenue des Champs-Elysées", 350, 35, 175, 500, 1100, 1300, 1500, 200) });
cases.push({ appartient: -1, element: new Taxe(100) });
cases.push({ appartient: -1, element: new Propriete(3, 8, "Rue de la Paix", 400, 50, 200, 600, 1400, 1700, 2000, 200) });

partie.cases = cases;


ipcMain.on('demarrerServeur', (event, port) => {
  ws = new WebSocket.Server({
    port: port
  });

  ws.on('connection', (client) => {
    let trouver = -1;
    if (partie.joueurs.length < 4) {
      let joueur = {
        pseudo: 'Joueur ' + partie.joueurs.length + 1,
        case: 0,
        pret: false,
        tour: 0,
        argent: 1500,
        billet1: 5,
        billet5: 1,
        billet10: 2,
        billet20: 1,
        billet50: 1,
        billet100: 4,
        billet500: 2,
        billet1000: 0,

        nbDouble: 0,


        proprietes: [],
        prison: false,
        testPrison: 0
      }
      partie.joueurs.push(joueur);
      partie.websocketJoueur.push(client);
      trouver = partie.joueurs.length - 1;
    }

    if (trouver == -1) {
      client.send(JSON.stringify({ "retour": "Plus de place disponible." }));
    }
    else {
      client.send(JSON.stringify({ code: 'nouveauJoueur', 'numJoueur': trouver }));
      UpdateJoueurs();

      client.on('message', (evt) => {
        var data = JSON.parse(evt);
        switch (data.code) {
          case 'lancerDes': { LancerDes(data.joueur); break; }
          case 'choixPion': {
            partie.joueurs[data.joueur].pseudo = data.pseudo;
            partie.joueurs[data.joueur].pion = data.pion;
            UpdateJoueurs();
            break;
          }
          case 'chat': { EnvoyerAToutLeMonde(data); break; }
          case 'pret': {
            partie.joueurs[data.numjoueur].pret = !partie.joueurs[data.numjoueur].pret;
            UpdateJoueurs();
            break;
          }
          case 'demarrer': {
            EnvoyerAToutLeMonde({ code: 'aQui', joueur: partie.aQuiLeTour });
            RepartirPropriete();
            break;
          }
          case 'suivant': { suivant(); break; }
          case 'quefaire': { QueFaire(data.joueur); break; }
          case 'validationChance': { ValidationChance(); break; }
          case 'validationCommunaute': { ValidationCommunaute(); break; }
          case 'acheter': { AcheterPropriete(data); break; }
          case 'suivant': { suivant(); break; }
          case 'sortirPrison': { SortirPrison(data); break; }
          case 'acheterMaison': { AcheterMaison(data); break; }
          case 'vendreMaison': { VendreMaison(data); break; }
          case 'validationQQ': { TomberChezJoueur(data.joueur); break; }
        }
      });
    }
  });
  event.sender.send('serveurDemarre');
});



function RepartirPropriete() {
  for (var i = 0; i < cases.length; i++) {
    if (cases[i].element instanceof Propriete ||
      cases[i].element instanceof Gare ||
      cases[i].element instanceof Service) {
      var random = Math.floor(Math.random() * partie.joueurs.length);

      cases[i].appartient = random;
      partie.joueurs[random].proprietes.push(cases[i].element);


    }
  }
  UpdateJoueurs();

}

function AcheterMaison(objet) {
  var possible = true;
  var propriete = objet.propriete;
  var numjoueur = objet.joueur;

  if (numjoueur != partie.aQuiLeTour) {
    EnvoyerMessageErreur(numjoueur, "Ce n'est pas à toi de jouer !");
    return;
  }

  var lapropriete = null;
  var lacase = 0;
  for (var i = 0; i < cases.length; i++) {
    if (cases[i].element instanceof Propriete) {
      // on regarde si toutes les propriétés du group sont possédées par le joueur
      if (cases[i].element.group == propriete.group) {
        if (cases[i].element.num == propriete.num) {
          lapropriete = cases[i].element;
          lacase = i;
        }

        if (cases[i].appartient != numjoueur) {
          possible = false;
        }
      }
    }
  }

  if (possible) {
    var achete = lapropriete.AcheterMaison(partie,partie.joueurs[numjoueur]);
    if (achete) {
      EnvoyerAToutLeMonde({ code: 'ajoutMaison', lacase: lacase });
      UpdateJoueurs();
    } else {
      EnvoyerMessageErreur(numjoueur, "Impossible d'ajouter une maison/hotel");
    }
  } else {
    EnvoyerMessageErreur(numjoueur, "Tu ne possèdes pas toutes les propriétés de ce groupe.");
  }

}

function VendreMaison(objet) {
  var joueur = partie.joueurs[objet.joueur];
  var propriete = objet.propriete;
  var lapropriete = null;
  var lacase = null;

  for (var i = 0; i < cases.length; i++) {
    if (cases[i].element instanceof Propriete) {
      // on regarde si toutes les propriétés du group sont possédées par le joueur
      if (cases[i].element.group == propriete.group) {
        if (cases[i].element.num == propriete.num) {
          lapropriete = cases[i].element;
          lacase = i;
        }
      }
    }
  }

  var vendu = lapropriete.VendreMaison(partie,joueur);
  if (vendu) {
    EnvoyerAToutLeMonde({ code: 'suppressionMaison', lacase: lacase });
    UpdateJoueurs();
  } else {
    EnvoyerMessageErreur(objet.joueur, "Il n'y a pas de maison à vendre.");
  }

}


function SortirPrison(objet) {
  partie.joueurs[objet.joueur].argent -= 50;
  partie.Payer(partie.joueurs[objet.joueur],50);
  partie.joueurs[objet.joueur].nbDouble = 0;
  partie.joueurs[objet.joueur].testPrison = 0;
  partie.joueurs[objet.joueur].prison = false;
  Envoyer1joueur(objet.joueur, { code: 'sortiePrison' });
  UpdateJoueurs();
  if (objet.joueur == partie.aQuiLeTour) {
    suivant();
  }
}

function AcheterPropriete(objet) {
  var joueur = objet.joueur;
  var casePropriete = cases[partie.joueurs[joueur].case];
  if (casePropriete.element.Acheter(partie,partie.joueurs[joueur])) {
    casePropriete.appartient = joueur;
    partie.joueurs[joueur].proprietes.push(casePropriete.element);
    UpdateJoueurs();
    suivant();
  } else {
    EnvoyerMessageErreur(objet.joueur, 'Achat impossible.');
    suivant();
  }
}

function EnvoyerMessageErreur(joueur, message) {
  Envoyer1joueur(joueur, { code: 'erreur', message: message });
}

// ici on fait les calculs pour le nombre de billet --------
function Payer(joueur, cout) {
  var montant = cout;
  while (montant >= 1000 && joueur.billet1000 > 0) {
    billet1000--;
    montant -= 1000;
  }

  while (montant >= 500 && joueur.billet500 > 0) {
    joueur.billet500--;
    montant -= 500;
  }

  while (montant >= 100 && joueur.billet100 > 0) {
    joueur.billet100--;
    montant -= 100;
  }
  while (montant >= 50 && joueur.billet50 > 0) {
    joueur.billet50--;
    montant -= 50;
  }
  while (montant >= 20 && joueur.billet20 > 0) {
    joueur.billet20--;
    montant -= 20;
  }
  while (montant >= 10 && joueur.billet10 > 0) {
    joueur.billet10--;
    montant -= 10;
  }
  while (montant >= 5 && joueur.billet5 > 0) {
    joueur.billet5--;
    montant -= 5;
  }
  while (montant >= 1 && joueur.billet1 > 0) {
    joueur.billet1--;
    montant -= 1;
  }

  if (montant != 0) // pas assez de monnaie
    RefaireMonnaie(joueur, montant);
}

function RefaireMonnaie(joueur, montantADevoir) {
  if (montantADevoir < 5) {

    if (joueur.billet5 > 0) MonnaieSur(joueur, 5);
    else if (joueur.billet10 > 0) MonnaieSur(joueur, 10);
    else if (joueur.billet20 > 0) MonnaieSur(joueur, 20);
    else if (joueur.billet50 > 0) MonnaieSur(joueur, 50);
    else if (joueur.billet100 > 0) MonnaieSur(joueur, 100);
    else if (joueur.billet500 > 0) MonnaieSur(joueur, 500);
    else if (joueur.billet1000 > 0) MonnaieSur(joueur, 1000);

  }
  else if (montantADevoir < 10) {
    if (joueur.billet10 > 0) MonnaieSur(joueur, 10);
    else if (joueur.billet20 > 0) MonnaieSur(joueur, 20);
    else if (joueur.billet50 > 0) MonnaieSur(joueur, 50);
    else if (joueur.billet100 > 0) MonnaieSur(joueur, 100);
    else if (joueur.billet500 > 0) MonnaieSur(joueur, 500);
    else if (joueur.billet1000 > 0) MonnaieSur(joueur, 1000);

  }
  else if (montantADevoir < 20) {
    if (joueur.billet20 > 0) MonnaieSur(joueur, 20);
    else if (joueur.billet50 > 0) MonnaieSur(joueur, 50);
    else if (joueur.billet100 > 0) MonnaieSur(joueur, 100);
    else if (joueur.billet500 > 0) MonnaieSur(joueur, 500);
    else if (joueur.billet1000 > 0) MonnaieSur(joueur, 1000);

  }
  else if (montantADevoir < 50) {
    if (joueur.billet50 > 0) MonnaieSur(joueur, 50);
    else if (joueur.billet100 > 0) MonnaieSur(joueur, 100);
    else if (joueur.billet500 > 0) MonnaieSur(joueur, 500);
    else if (joueur.billet1000 > 0) MonnaieSur(joueur, 1000);

  }
  else if (montantADevoir < 100) {
    if (joueur.billet100 > 0) MonnaieSur(joueur, 100);
    else if (joueur.billet500 > 0) MonnaieSur(joueur, 500);
    else if (joueur.billet1000 > 0) MonnaieSur(joueur, 1000);

  }
  else if (montantADevoir < 500) {
    if (joueur.billet500 > 0) MonnaieSur(joueur, 500);
    else if (joueur.billet1000 > 0) MonnaieSur(joueur, 1000);

  }
  else if (montantADevoir < 1000) {
    if (joueur.billet1000 > 0) MonnaieSur(joueur, 1000);

  }
  Payer(joueur,montantADevoir);

}

function MonnaieSur(joueur, montant) {
  if (montant == 1000) {
    joueur.billet1000--;
    joueur.billet500++;
    joueur.billet100 += 2;
    joueur.billet50 += 4;
    joueur.billet20 += 5;
  }

  else if (montant == 500) {
    joueur.billet500--;
    joueur.billet100 += 2;
    joueur.billet50 += 4;
    joueur.billet20 += 5;
  }
  else if (montant == 100) {
    joueur.billet100--;
    joueur.billet50++;
    joueur.billet20++;
    joueur.billet10++;
    joueur.billet5 += 4;
  }
  else if (montant == 50) {
    joueur.billet50--;
    joueur.billet20++;
    joueur.billet10++;
    joueur.billet5 += 2;
    joueur.billet1 += 10;
  }
  else if (montant == 20) {
    joueur.billet20--;
    joueur.billet10++;
    joueur.billet5++;
    joueur.billet1 += 5;
  }
  else if (montant == 10) {
    joueur.billet10--;
    joueur.billet5++;
    joueur.billet1 += 5;
  }
  else if (montant == 5) {
    joueur.billet5--;
    joueur.billet1 += 5;
  }
}

function GagnerArgent(joueur, cout) {
  var montant = cout;
  while (cout >= 1000) {
    joueur.billet1000++;
    cout -= 1000;
  }

  while (cout >= 500) {
    joueur.billet500++;
    cout -= 500;
  }

  while (cout >= 100) {
    joueur.billet100++;
    cout -= 100;
  }
  while (cout >= 50) {
    joueur.billet50++;
    cout -= 50;
  }
  while (cout >= 20) {
    joueur.billet20++;
    cout -= 20;
  }
  while (cout >= 10) {
    joueur.billet10++;
    cout -= 10;
  }
  while (cout >= 5) {
    joueur.billet5++;
    cout -= 5;
  }
  while (cout >= 1) {
    joueur.billet1++;
    cout -= 1;
  }
}
// ---------------------------------------------------------

function InformerDeplacement(joueur, nouvellecase) {
  EnvoyerAToutLeMonde({ code: 'deplacementJoueur', joueur: joueur, nouvelleCase: nouvellecase });
}

function ValidationChance() {
  var ancienneCase = partie.joueurs[partie.aQuiLeTour].case;

  cases[ancienneCase].element.derniereCarte.Action(partie);

  var nouvelleCase = partie.joueurs[partie.aQuiLeTour].case;

  if (ancienneCase != nouvelleCase) {
    InformerDeplacement(partie.aQuiLeTour, nouvelleCase);
  }
  else {
    UpdateJoueurs();
    suivant();
  }
}

function ValidationCommunaute() {
  var ancienneCase = partie.joueurs[partie.aQuiLeTour].case;

  cases[ancienneCase].element.derniereCarte.Action(partie);

  var nouvelleCase = partie.joueurs[partie.aQuiLeTour].case;

  if (ancienneCase != nouvelleCase) {
    InformerDeplacement(partie.aQuiLeTour, nouvelleCase);

  }
  else {
    UpdateJoueurs();
    suivant();
  }

}

function suivant(forceMemeJoueur) {
  var forcage = (forceMemeJoueur === undefined) ? false : forceMemeJoueur;
  if (partie.joueurs[partie.aQuiLeTour].nbDouble > 0 || forceMemeJoueur) {
    // on ne change pas de joueur.
  } else {
    partie.aQuiLeTour++;
    if (partie.aQuiLeTour >= partie.joueurs.length)
      partie.aQuiLeTour = 0;
  }
  EnvoyerAToutLeMonde({ code: 'aQui', joueur: partie.aQuiLeTour });

}

function Envoyer1joueur(joueur, objet) {
  partie.websocketJoueur[joueur].send(JSON.stringify(objet));
}

function TomberChezJoueurMessage(joueur, lacase) {
  var cout = lacase.element.Cout(partie);

  Envoyer1joueur(joueur, {
    code: 'chezQuelquun', message: 'Vous êtes chez ' +
      partie.joueurs[lacase.appartient].pseudo + ', vous lui devez ' + cout + '€'
  });

}

function TomberChezJoueur(joueur) {
  var lacase = partie.joueurs[joueur].case;
  var propriete = cases[lacase];
  propriete.element.Payer(partie, partie.joueurs[joueur], partie.joueurs[propriete.appartient]);
  UpdateJoueurs();
  suivant();
}

function QueFaire(joueur) {

  if (partie.joueurs[joueur].nbDouble < 3) {
    if (partie.joueurs[joueur].prison) {

      // un double a été fait alors le joueur peux sortir de prison.
      if (partie.joueurs[joueur].nbDouble > 0) {
        partie.joueurs[joueur].prison = false;
        partie.joueurs[joueur].testPrison = 0;
        partie.joueurs[joueur].nbDouble = 0;
        Envoyer1joueur(joueur, { code: 'sortiePrison' });
        UpdateJoueurs();
        suivant(true);
      } else {
        partie.joueurs[joueur].testPrison++;
        UpdateJoueurs();
        suivant();
      }

    } else {
      var caseTombe = cases[partie.joueurs[joueur].case];

      if (caseTombe.element instanceof Propriete || caseTombe.element instanceof Gare || caseTombe.element instanceof Service) {

        // appartient à personne
        if (caseTombe.appartient == -1) {
          // si premier tour on peux rien faire sinon il peut acheter
          if (partie.joueurs[joueur].tour > 0) {
            Envoyer1joueur(joueur, { code: 'achatPossible', propriete: caseTombe.element });

          } else {
            suivant();
          }

        } else {
          // si elle appartient à quelqu'un alors il faut faire payer le jouer à l'autre joueur
          if (caseTombe.appartient != joueur ){ // || true) {
            TomberChezJoueurMessage(joueur, caseTombe);
          } else {
            suivant();
          }

        }

      } else {
        if (caseTombe.element instanceof Taxe) {
          partie.joueurs[joueur].argent -= caseTombe.element.montant;
          partie.Payer(partie.joueurs[joueur],caseTombe.element.montant);
          partie.park += caseTombe.element.montant;
          UpdateJoueurs();
          suivant();
        }

        if (caseTombe.element instanceof Chance) {
          var carte = caseTombe.element.TirerCarte();
          carte.Message(partie);

        }

        if (caseTombe.element instanceof Communaute) {
          var carte = caseTombe.element.TirerCarte();
          carte.Message(partie);

        }

        if (caseTombe.element instanceof Case) {
          if (caseTombe.element.typeCase == 1) {
            partie.joueurs[joueur].argent += 200;
            partie.GagnerArgent(partie.joueurs[joueur],200);
            UpdateJoueurs();
            suivant();

          } else if (caseTombe.element.typeCase == 3) {
            partie.joueurs[joueur].argent += partie.park;
            partie.GagnerArgent(partie.joueurs[joueur],partie.park);
            partie.park = 0;
            
            UpdateJoueurs();
            suivant();

          } else if (caseTombe.element.typeCase == 4) {
            partie.joueurs[joueur].case = 10;
            partie.joueurs[joueur].prison = true;
            partie.joueurs[joueur].nbDouble = 0;
            partie.joueurs[joueur].testPrison = 0;
            InformerDeplacement(joueur, 10);
            Envoyer1joueur(joueur, { code: 'prison' });
          }
          else {
            suivant();

          }
        }

      }
    }


  } else {
    partie.joueurs[joueur].nbDouble = 0;
    partie.joueurs[joueur].case = 10;
    partie.joueurs[joueur].prison = true;
    partie.joueurs[joueur].testPrison = 0;
    InformerDeplacement(joueur, partie.joueurs[joueur].case);
    Envoyer1joueur(joueur, { code: 'prison' });
    UpdateJoueurs();
    suivant();

  }

}

function UpdateJoueurs() {
  var lescases = [];
  for(var i=0;i<cases.length;i++){
    lescases.push(cases[i].appartient);
  }

  EnvoyerAToutLeMonde({ code: 'majJoueurs', joueurs: partie.joueurs, lescases: lescases });
  EnvoyerAToutLeMonde({ code: 'updateParcGratuit', montant: partie.park });
}

function EnvoyerAToutLeMonde(objet) {
  for (var i = 0; i < partie.websocketJoueur.length; i++) {
    partie.websocketJoueur[i].send(JSON.stringify(objet))
  }
}

function LancerDes(joueur) {
  var lancer1 = Math.floor(Math.random() * 6) + 1;
  var lancer2 = Math.floor(Math.random() * 6) + 1;

  if (lancer1 == lancer2) {
    partie.joueurs[joueur].nbDouble++;
  }
  else {
    partie.joueurs[joueur].nbDouble = 0;
  }

  // si le joueur est en prison
  if (partie.joueurs[joueur].prison) {


  } else { // il n'est pas en prison

    if (partie.joueurs[joueur].nbDouble == 3) { // il a fait trois double alors ce sera la prison.

    } else {
      partie.joueurs[joueur].case += lancer1 + lancer2;

      partie.joueurs[joueur].dernierLance = lancer1 + lancer2;

      if (partie.joueurs[joueur].case >= 40) {
        partie.joueurs[joueur].case -= 40;
        partie.joueurs[joueur].tour++;
        partie.joueurs[joueur].argent += 200;
        EnvoyerMessageErreur(joueur, "Plus 200€ pour être passé par la case départ.");

        partie.GagnerArgent(partie.joueurs[joueur],200);


      }
      // force la prison 
      // partie.joueurs[joueur].case = 30;

    }
  }

  UpdateJoueurs();


  EnvoyerAToutLeMonde({ code: 'lancerDes', joueur: joueur, des1: lancer1, des2: lancer2, nouvelleCase: partie.joueurs[joueur].case });
}

function createWindow() {
  // Créer le browser window.
  win = new BrowserWindow({ width: 1280, height: 720, autoHideMenuBar: true })

  // et charge le index.html de l'application.
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  win.webContents.openDevTools()



  // Émit lorsque la fenêtre est fermée.
  win.on('closed', () => {
    // Dé-référence l'objet window , normalement, vous stockeriez les fenêtres
    // dans un tableau si votre application supporte le multi-fenêtre. C'est le moment
    // où vous devez supprimer l'élément correspondant.
    win = null
  })

}

// Cette méthode sera appelée quant Electron aura fini
// de s'initialiser et sera prêt à créer des fenêtres de navigation.
// Certaines APIs peuvent être utilisées uniquement quant cet événement est émit.
app.on('ready', createWindow)

// Quitte l'application quand toutes les fenêtres sont fermées.
app.on('window-all-closed', () => {
  // Sur macOS, il est commun pour une application et leur barre de menu
  // de rester active tant que l'utilisateur ne quitte pas explicitement avec Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // Sur macOS, il est commun de re-créer une fenêtre de l'application quand
  // l'icône du dock est cliquée et qu'il n'y a pas d'autres fenêtres d'ouvertes.
  if (win === null) {
    createWindow()
  }
})
