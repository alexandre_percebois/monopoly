
function Case(typeCase) {
    // 1 depart
    // 2 simple visite
    // 3 prison
    // 4 park
    this.typeCase = typeCase;
}

function Gare(num,libelle) {
    this.group=10;
    this.num = num;
    this.type = 'Gare';
    this.achat = 200;
    this.titre = libelle;

    this.Acheter = (partie,joueur) => {
        if (joueur.argent > this.achat) {
            joueur.argent -= this.achat;
            partie.Payer(joueur,this.achat);
            return true;
        }
        return false;
    }

    this.Cout = (partie) => {
        var qui = partie.joueurs[partie.aQuiLeTour];
        var lacase = partie.cases[qui.case];

        var quipossede = lacase.appartient;

        var combien = 0;
        if (partie.cases[5].appartient == quipossede) combien++;
        if (partie.cases[15].appartient == quipossede) combien++;
        if (partie.cases[25].appartient == quipossede) combien++;
        if (partie.cases[35].appartient == quipossede) combien++;

        return Math.pow(2, combien - 1) * 25;


    }

    this.Payer = (partie, joueur1, joueur2) => {
        var cout = this.Cout(partie);
        joueur1.argent -= cout;
        joueur2.argent += cout;
        partie.Payer(joueur1,cout);
        partie.GagnerArgent(joueur2,cout);
        return cout;

    }
}

function Service(num,libelle) {
    this.group=9;
    this.num = num;

    this.type = 'Service';
    this.achat = 150;
    this.titre = libelle;
    this.Acheter = (partie,joueur) => {
        if (joueur.argent > this.achat) {
            joueur.argent -= this.achat;
            partie.Payer(joueur,this.achat);

            return true;
        }
        return false;
    }
    this.Cout = (partie) => {
        var qui = partie.joueurs[partie.aQuiLeTour];
        var lacase = partie.cases[qui.case];
        var quipossede = lacase.appartient;

        var combien = 0;
        if (partie.cases[12].appartient == quipossede) combien++;
        if (partie.cases[28].appartient == quipossede) combien++;

        if (combien == 2) {
            return 10 * qui.dernierLance;
        }

        return 4 * qui.dernierLance;

    }

    this.Payer = (partie, joueur1, joueur2) => {
        var cout = this.Cout(partie);
        joueur1.argent -= cout;
        joueur2.argent += cout;
        partie.Payer(joueur1,cout);
        partie.GagnerArgent(joueur2,cout);
        return cout;
    }
}

function Taxe(montant) {

    this.montant = montant;
}

function Chance(carteChances) {
    this.carteChances = carteChances;
    this.derniereCarte = null;

    this.TirerCarte = () => {
        var carte = this.carteChances.splice(0, 1)[0];
        this.carteChances.push(carte);
        this.derniereCarte = carte;
        return carte;
    };
}

function Communaute(carteCommunaute) {
    this.carteCommunaute = carteCommunaute;
    this.derniereCarte = null;

    this.TirerCarte = () => {
        var carte = this.carteCommunaute.splice(0, 1)[0];
        this.carteCommunaute.push(carte);
        this.derniereCarte = carte;

        return carte;
    };

}

function Propriete(num, group, titre, achat, nu, maison1, maison2, maison3, maison4, hotel, prixmaison) {
    this.type = 'Propriete';

    this.num = num;
    this.group = group;
    this.titre = titre;
    this.achat = achat;
    this.nu = nu;
    this.maison1 = maison1;
    this.maison2 = maison2;
    this.maison3 = maison3;
    this.maison4 = maison4;
    this.hotel = hotel;
    this.prixmaison = prixmaison;

    this.nbMaison = 0;

    this.Cout = (partie) => {
        var cout = this.nu;
        if (this.nbMaison == 1) cout = this.maison1;
        if (this.nbMaison == 2) cout = this.maison2;
        if (this.nbMaison == 3) cout = this.maison3;
        if (this.nbMaison == 4) cout = this.maison4;
        if (this.nbMaison == 5) cout = this.hotel;
        return cout;
    }

    this.Acheter = (partie,joueur) => {
        if (joueur.argent > this.achat) {
            joueur.argent -= this.achat;
            partie.Payer(joueur,this.achat);
            return true;
        }
        return false;
    }

    this.Payer = (partie, joueur1, joueurPossedant) => {
        var cout = this.Cout(partie);
        joueur1.argent -= cout;
        joueurPossedant.argent += cout;

        partie.Payer(joueur1,this.achat);
        partie.GagnerArgent(joueurPossedant,this.achat);

        return cout;
    }

    this.AcheterMaison = (partie, joueur) => {
        if (this.nbMaison < 5) {
            if (joueur.argent > this.prixmaison) {
                joueur.argent -= this.prixmaison;
                partie.Payer(joueur,this.prixmaison);
                this.nbMaison++;
                return true;
            }
        }
        return false;
    }

    this.VendreMaison = (partie, joueur) => {
        if (this.nbMaison > 0) {
            joueur.argent += this.prixmaison;
            partie.GagnerArgent(joueur,this.prixmaison);
            this.nbMaison--;
            return true;
        }
        return false;
    }
}

module.exports = {
    Case: Case,
    Gare: Gare,
    Service: Service,
    Taxe: Taxe,
    Chance: Chance,
    Communaute: Communaute,
    Propriete: Propriete
};