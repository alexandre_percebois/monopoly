function CarteChance(type, libelle, action, action2) {

    // 1 = avancer a
    // 2 reculer de
    // 3 gagner
    // 4 payer
    // 5 payer par maison
    // 6 prison
    this.type = type;
    this.libelle = libelle;
    this.action = action;
    this.action2 = action2;
    this.Message = (partie) => {
        partie.websocketJoueur[partie.aQuiLeTour].send(JSON.stringify({
            code: 'chance',
            message: this.libelle
        }));
    };
    this.Action = (partie) => {
        switch (this.type) {
            case 1: {
                partie.joueurs[partie.aQuiLeTour].case = this.action;
                break;
            }
            case 2: {
                partie.joueurs[partie.aQuiLeTour].case -= this.action;
                if (partie.joueurs[partie.aQuiLeTour].case < 0)
                    partie.joueurs[partie.aQuiLeTour].case += 40;
                break;
            }
            case 3: {
                partie.joueurs[partie.aQuiLeTour].argent += this.action;
                partie.GagnerArgent(partie.joueurs[partie.aQuiLeTour],this.action);
                break;
            }
            case 4: {
                partie.joueurs[partie.aQuiLeTour].argent -= this.action;
                partie.park += this.action;
                partie.Payer(partie.joueurs[partie.aQuiLeTour],this.action);
                break;
            }
            case 5: {

                break;
            }
            case 6: {
                partie.joueurs[partie.aQuiLeTour].case = 10;
                partie.joueurs[partie.aQuiLeTour].prison = true;
                break;
            }
        }



    }

}

function CarteCommunaute(type, libelle, action) {
    // 1 payer
    // 2 gagner
    // 3 prison
    // 4 casedepart
    // 5 retourner à 
    // 6 anniversaire
    this.type = type;
    this.libelle = libelle;
    this.action = action;

    this.Message = (partie) => {
        partie.websocketJoueur[partie.aQuiLeTour].send(JSON.stringify({
            code: 'communaute',
            message: this.libelle
        }));

    };
    this.Action = (partie) => {
        switch (this.type) {
            case 1: {
                partie.joueurs[partie.aQuiLeTour].argent -= this.action;
                partie.park += this.action;
                partie.Payer(partie.joueurs[partie.aQuiLeTour],this.action);
                break;
            }
            case 2: {
                partie.joueurs[partie.aQuiLeTour].argent += this.action;
                partie.GagnerArgent(partie.joueurs[partie.aQuiLeTour],this.action);
                break;
            }
            case 3: {
                partie.joueurs[partie.aQuiLeTour].case = 10;
                partie.joueurs[partie.aQuiLeTour].prison = true;
                break;
            }
            case 4: {
                partie.joueurs[partie.aQuiLeTour].case = 0;
                partie.joueurs[partie.aQuiLeTour].argent += 200;
                partie.GagnerArgent(partie.joueurs[partie.aQuiLeTour],200);
                break;
            }
            case 5: {
                partie.joueurs[partie.aQuiLeTour].case = this.action;
                break;
            }
            case 6: {
                for (var i = 0; i < partie.joueurs.length; i++) {
                    if (i != partie.aQuiLeTour) {
                        partie.joueurs[partie.aQuiLeTour].argent += this.action;
                        partie.GagnerArgent(partie.joueurs[partie.aQuiLeTour],this.action);
                        partie.joueurs[i].argent -= this.action;
                        partie.Payer(partie.joueurs[i],this.action);
                    }
                }
                break;
            }
        }

    }
}

module.exports = {
    CarteChance: CarteChance,
    CarteCommunaute: CarteCommunaute
};