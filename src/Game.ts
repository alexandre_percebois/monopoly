
import {
    Engine, Scene,
    GameObject, Mesh,
    VertexData,
    Light,
    LIGHT_TYPE,
    Material,
    GLTFLoader,
    Entity,
    AnimatedValue,
    AnimationChannel,
    Animation
} from 'percengine/index';

import { vec3, quat } from 'gl-matrix';
import { constants } from 'zlib';
const electron = require('electron');


export class Game extends Scene {

    private engine: Engine | null;

    private objet: GameObject;
    private des: GameObject;
    private des2: GameObject;

    private angle = 0;
    private lumiere: Light;
    private variante = -0.01;

    private phi = 1.11; // Math.PI / 2.0;
    private theta = -1.56; // Math.PI / 2.0;
    private precedentPosition = [0, 0];
    private radius = 5;

    private billet1: GameObject;
    private billet5: GameObject;
    private billet10: GameObject;
    private billet20: GameObject;
    private billet50: GameObject;
    private billet100: GameObject;
    private billet500: GameObject;
    private billet1000: GameObject;

    private maison: GameObject;

    private websocket: WebSocket;
    private numJoueur: number;


    private casePropriete: any = [];

    private joueurs: any = [null, null, null, null];

    private billetsJoueurs: any = [];

    private maisonPlacees: any = [];

    private groupeProprietes: any = {
        groupe: []
    };

    constructor() {
        super();
        this.engine = new Engine(this);
    }

    OnInit() {
        super.OnInit();

        const loaderGLTF = new GLTFLoader();
        this.objet = loaderGLTF.OpenFile('./src/objects/Scene.gltf');
        this.objet.SetParent(this);

        this.des = loaderGLTF.OpenFile('./src/objects/Des_out/Des.gltf');
        this.AddChild(this.des);
        this.des.SetVisible(false);

        let anim1 = this.des.GetAnimation(0).Split(0, 2); // 1
        let anim2 = this.des.GetAnimation(0).Split(8.2, 11.2); // 2
        let anim3 = this.des.GetAnimation(0).Split(11.2, 13); // 3
        let anim4 = this.des.GetAnimation(0).Split(2, 4.2); // 4
        let anim5 = this.des.GetAnimation(0).Split(4.2, 6.2); // 5
        let anim6 = this.des.GetAnimation(0).Split(6.2, 8.2); // 6

        this.des.AddAnimation(anim1);
        this.des.AddAnimation(anim2);
        this.des.AddAnimation(anim3);
        this.des.AddAnimation(anim4);
        this.des.AddAnimation(anim5);
        this.des.AddAnimation(anim6);


        this.des2 = loaderGLTF.OpenFile('./src/objects/Des_out/Des.gltf');
        this.AddChild(this.des2);
        this.des2.SetPosition(2, 0, 1);
        this.des2.SetVisible(false);
        anim1 = this.des2.GetAnimation(0).Split(0, 2); // 1
        anim2 = this.des2.GetAnimation(0).Split(8.2, 11.2); // 2
        anim3 = this.des2.GetAnimation(0).Split(11.2, 13); // 3
        anim4 = this.des2.GetAnimation(0).Split(2, 4.2); // 4
        anim5 = this.des2.GetAnimation(0).Split(4.2, 6.2); // 5
        anim6 = this.des2.GetAnimation(0).Split(6.2, 8.2); // 6

        this.des2.AddAnimation(anim1);
        this.des2.AddAnimation(anim2);
        this.des2.AddAnimation(anim3);
        this.des2.AddAnimation(anim4);
        this.des2.AddAnimation(anim5);
        this.des2.AddAnimation(anim6);

        this.billet1 = loaderGLTF.OpenFile('./src/objects/Billet1_out/Billet1.gltf');
        this.billet1.SetVisible(false);
        this.billet5 = loaderGLTF.OpenFile('./src/objects/Billet5_out/Billet5.gltf');
        this.billet5.SetVisible(false);
        this.billet10 = loaderGLTF.OpenFile('./src/objects/Billet10_out/Billet10.gltf');
        this.billet10.SetVisible(false);
        this.billet20 = loaderGLTF.OpenFile('./src/objects/Billet20_out/Billet20.gltf');
        this.billet20.SetVisible(false);
        this.billet50 = loaderGLTF.OpenFile('./src/objects/Billet50_out/Billet50.gltf');
        this.billet50.SetVisible(false);
        this.billet100 = loaderGLTF.OpenFile('./src/objects/Billet100_out/Billet100.gltf');
        this.billet100.SetVisible(false);
        this.billet500 = loaderGLTF.OpenFile('./src/objects/Billet500_out/Billet500.gltf');
        this.billet500.SetVisible(false);
        this.billet1000 = loaderGLTF.OpenFile('./src/objects/Billet1000_out/Billet1000.gltf');
        this.billet1000.SetVisible(false);


        this.maison = loaderGLTF.OpenFile('./src/objects/Maison_out/Maison.gltf');
        this.maison.SetVisible(false);

        electron.ipcRenderer.on('serveurDemarre', (evt: any, arg: any) => {
            this.RejoindrePartie('localhost');
        });

        document.getElementById('CreerPartie').onclick = () => {
            electron.ipcRenderer.send('demarrerServeur', 12345);
        };

        document.getElementById('RejoindrePartie').onclick = () => {
            document.getElementById('backdialog').style.display = 'flex';
            document.getElementById('formJoin').style.display = 'flex';
            document.getElementById('formJoin').onsubmit = (form) => {
                if ((<HTMLFormElement>form.target).checkValidity()) {
                    this.RejoindrePartie((<any>form.target).ip_join.value);
                    document.getElementById('formJoin').style.display = 'none';
                    document.getElementById('backdialog').style.display = 'none';
                } else {
                    document.getElementById('joinErreur').innerHTML = 'Champ requis manquant';
                }
                return false;
            };
        };

        document.getElementById('choixPion').onsubmit = (evt) => {
            this.websocket.send(JSON.stringify({
                code: 'choixPion',
                joueur: this.numJoueur,
                pseudo: (<any>evt.target).pseudo.value,
                pion: parseInt((<any>evt.target).pion.value, 0),
            }));
            this.HideDialog('choixPion');
            document.getElementById('partiHUD').style.display = 'block';
            return false;
        };

        document.getElementById('saisieChat').onsubmit = (evt) => {
            const form = <HTMLFormElement>evt.target;
            this.websocket.send(JSON.stringify({
                code: 'chat',
                joueur: this.numJoueur,
                content: form.texte.value
            }));
            form.texte.value = '';
            return false;

        };

        this.lumiere = new Light(LIGHT_TYPE.POINT);
        this.lumiere.SetParent(this);
        this.lumiere.SetPosition(0, 2, 0);
        this.lumiere.SetLightColor(1, 1, 1);


    }

    HideDialog(htmlid: string) {
        document.getElementById('backdialog').style.display = 'none';
        document.getElementById(htmlid).style.display = 'none';

    }

    ShowMessage(message: string) {
        for (const child of document.getElementById('backdialog').children) {
            (child as HTMLElement).style.display = 'none';
        }


        document.getElementById('popupMessage').style.display = 'flex';
        document.getElementById('backdialog').style.display = 'flex';
        document.getElementById('messageContent').innerHTML = message;
    }


    RejoindrePartie(ip: string) {
        this.websocket = new WebSocket('ws://' + ip + ':12345');
        this.websocket.onmessage = (message: MessageEvent) => {
            const objet = JSON.parse(message.data);
            if (objet.retour !== undefined) {
                this.ShowMessage(objet.retour);
            } else {

                switch (objet.code) {
                    case 'contentJoueur': { break; }
                    case 'erreur': { this.AjouterNotification(objet.message); break; }
                    case 'nouveauJoueur': {
                        this.numJoueur = objet.numJoueur; document.getElementById('menu').style.display = 'none';
                        this.ShowDialog('choixPion');
                        break;
                    }
                    case 'majJoueurs': { this.UpdateJoueurs(objet.joueurs, objet.lescases); break; }
                    case 'chat': { this.UpdateChat(objet); break; }
                    case 'aQui': { this.AQui(objet.joueur); break; }
                    case 'lancerDes': { this.LancerDesResultat(objet); break; }
                    case 'chance': { this.DisplayChance(objet); break; }
                    case 'communaute': { this.DisplayCommunaute(objet); break; }
                    case 'deplacementJoueur': { this.DeplacementJoueur(objet); break; }
                    case 'achatPossible': { this.AchatPossible(objet); break; }
                    case 'prison': { this.ShowMessage('Vous êtes en prison.'); break; }
                    case 'sortiePrison': { this.ShowMessage('Vous êtes sortie de prison.'); break; }
                    case 'ajoutMaison': { this.AjouterMaison(objet); break; }
                    case 'chezQuelquun': { this.ChezQuelqun(objet.message); break; }
                    case 'suppressionMaison': { this.VenteMaison(objet); break; }
                    case 'updateParcGratuit': { this.UpdateParcGratuit(objet.montant); break; }

                }
            }
        };
    }

    AjouterNotification(message: string) {
        const container = document.getElementById('notification');

        const divNotif = document.createElement('div');
        divNotif.className = 'notification';
        divNotif.innerHTML = message;
        container.appendChild(divNotif);
        setTimeout(() => {
            divNotif.className += ' hide';

            setTimeout(() => {
                container.removeChild(divNotif);
            }, 500);

        }, 3000);

    }

    UpdateParcGratuit(montant: number) {
        document.getElementById('parc').innerHTML = montant + '€';
    }


    ValidationQQ() {
        this.SendToServer({ code: 'validationQQ', joueur: this.numJoueur });
        this.HideDialog('chezQQ');
    }

    ChezQuelqun(message: any) {
        document.getElementById('chezQQ').style.display = 'flex';
        document.getElementById('backdialog').style.display = 'flex';
        document.getElementById('chezQQ').querySelector('#messageContent').innerHTML = message;
    }

    VenteMaison(objet: any) {
        if (this.maisonPlacees[objet.lacase] !== undefined) {
            const maisonP = this.maisonPlacees[objet.lacase];
            maisonP.nbMaison--;

            const object = maisonP.objets.splice(-1, 1)[0];
            object.Destroy();

            if (maisonP.nbMaison === 4) {
                for (let i = 0; i < 4; i++) {
                    maisonP.objets.push(this.FairePopMaison([0, 1, 0], i, objet.lacase));
                }
            }
        }
    }

    FairePopMaison(couleur: Array<number>, quelmaison: number, lacase: number) {
        const nouvelleMaison = this.maison.CreateInstance(true);
        nouvelleMaison.SetVisible(true);
        const mesh = nouvelleMaison.GetMeshByName('Maison');
        if (mesh != null) {
            mesh.GetVertexData(0).GetMaterial().SetColor(couleur[0], couleur[1], couleur[2], 1);
        }

        const positionMaison = this.GetChildByName('MaisonCase' + this.pad(lacase, 3));
        positionMaison.SetScale(1, 1, 1);

        const position = [0, 0, 0];
        position[1] += 0.07 * (quelmaison - 0.4);

        nouvelleMaison.SetPosition(position[0], position[1], position[2]);
        nouvelleMaison.SetRotation(1, 0, 0, 90);
        const animation = new Animation();
        const animatedChannel = new AnimationChannel();
        animation.AddChannel(animatedChannel);

        nouvelleMaison.GetPosition().SetAnimation(animatedChannel);
        nouvelleMaison.AddAnimation(animation);

        animatedChannel.AddTimingAndValue(0, [position[0], position[1], position[2] - 1]);
        animatedChannel.AddTimingAndValue(1, [position[0], position[1], position[2]]);
        animation.SetEnd(1);
        animation.Play();
        nouvelleMaison.SetParent(positionMaison);

        return nouvelleMaison;
    }

    AjouterMaison(objet: any) {
        if (this.maisonPlacees[objet.lacase] === undefined) {
            this.maisonPlacees[objet.lacase] = {
                nbMaison: 0,
                objets: []
            };
        }

        let nouvelleMaison: GameObject;
        // on ajout l'hotel
        if (this.maisonPlacees[objet.lacase].nbMaison === 4) {
            nouvelleMaison = this.FairePopMaison([1, 0, 0], 2.5, objet.lacase);

            for (const mais of this.maisonPlacees[objet.lacase].objets) {
                mais.Destroy();
            }
            this.maisonPlacees[objet.lacase].objets = [];
        } else {
            nouvelleMaison = this.FairePopMaison([0, 1, 0], this.maisonPlacees[objet.lacase].nbMaison, objet.lacase);
        }


        this.maisonPlacees[objet.lacase].nbMaison++;
        this.maisonPlacees[objet.lacase].objets.push(nouvelleMaison);

    }

    RemplirPropriete(propriete: any, div: HTMLDivElement) {
        if (propriete.type === 'Propriete') {
            div.querySelector('#titrePropriete').innerHTML = propriete.titre;

            switch (propriete.group) {
                case 1: { (div.querySelector('#titrePropriete') as HTMLDivElement).style.backgroundColor = '#945434'; break; }
                case 2: { (div.querySelector('#titrePropriete') as HTMLDivElement).style.backgroundColor = '#aadffd'; break; }
                case 3: { (div.querySelector('#titrePropriete') as HTMLDivElement).style.backgroundColor = '#d83a9a'; break; }
                case 4: { (div.querySelector('#titrePropriete') as HTMLDivElement).style.backgroundColor = '#f69321'; break; }
                case 5: { (div.querySelector('#titrePropriete') as HTMLDivElement).style.backgroundColor = '#ea1e23'; break; }
                case 6: { (div.querySelector('#titrePropriete') as HTMLDivElement).style.backgroundColor = '#fcf400'; break; }
                case 7: { (div.querySelector('#titrePropriete') as HTMLDivElement).style.backgroundColor = '#1fb25b'; break; }
                case 8: { (div.querySelector('#titrePropriete') as HTMLDivElement).style.backgroundColor = '#0170bd'; break; }
            }

            div.querySelector('#nu').innerHTML = propriete.nu + '€';
            div.querySelector('#maison1').innerHTML = propriete.maison1 + '€';
            div.querySelector('#maison2').innerHTML = propriete.maison2 + '€';
            div.querySelector('#maison3').innerHTML = propriete.maison3 + '€';
            div.querySelector('#maison4').innerHTML = propriete.maison4 + '€';
            div.querySelector('#hotel').innerHTML = propriete.hotel + '€';
            div.querySelector('#achat').innerHTML = propriete.achat + '€';
            div.querySelector('#prixmaison').innerHTML = propriete.prixmaison + '€';
        } else if (propriete.type === 'Gare') {
            div.querySelector('#titreGare').innerHTML = propriete.titre;
        } else if (propriete.type === 'Gare') {
            div.querySelector('#titreGare').innerHTML = propriete.titre;
        }
    }

    AchatPossible(objet: any) {

        switch (objet.propriete.type) {
            case 'Gare': {
                document.getElementById('titreGare').innerHTML = objet.propriete.titre;
                this.ShowDialog('achatGare');
                break;
            }
            case 'Propriete': {
                this.ShowDialog('achatPropriete');
                this.RemplirPropriete(objet.propriete, document.getElementById('achatPropriete') as HTMLDivElement);
                break;
            }
            case 'Service': {
                document.getElementById('titreService').innerHTML = objet.propriete.titre;
                this.ShowDialog('achatService');
                break;
            }
        }
    }

    Acheter() {
        this.SendToServer({ code: 'acheter', joueur: this.numJoueur });
    }

    DeplacementJoueur(objet: any) {
        const joueur = this.joueurs[objet.joueur];
        const pion = <GameObject>joueur.pion;

        joueur.case = objet.nouvelleCase;

        const chaineCase = 'Case' + this.pad(joueur.case, 3);
        const dummyCase = this.GetChildByName(chaineCase);

        const position = dummyCase.GetPosition().GetValue();

        const actuelle = pion.GetPosition().GetValue();

        pion.GetPosition().GetAnimationChannel().Clear();
        pion.GetPosition().GetAnimationChannel().AddTimingAndValue(0, [
            actuelle[0],
            actuelle[1],
            actuelle[2]
        ]);

        pion.GetPosition().GetAnimationChannel().AddTimingAndValue(0.5, [
            (actuelle[0] + position[0]) / 2,
            0.5,
            (actuelle[2] + position[2]) / 2
        ]);

        pion.GetPosition().GetAnimationChannel().AddTimingAndValue(1, [
            position[0], position[1], position[2]
        ]);
        pion.GetAnimation(0).SetEndEvent(() => {
            if (objet.joueur === this.numJoueur) {
                this.SendToServer({ code: 'quefaire', joueur: this.numJoueur });
            }
            pion.GetAnimation(0).SetEndEvent(null);
        });
        pion.GetAnimation(0).SetEnd(1);
        pion.GetAnimation(0).Reset();
        pion.GetAnimation(0).Play();
    }

    ValidationChance() {
        this.SendToServer({ code: 'validationChance' });
        this.HideDialog('chance');
    }
    ValidationCommunaute() {
        this.SendToServer({ code: 'validationCommunaute' });
        this.HideDialog('communaute');
    }

    DisplayChance(objet: any) {
        this.ShowDialog('chance');
        document.getElementById('chance').querySelector('#message').innerHTML = objet.message;
    }

    DisplayCommunaute(objet: any) {
        this.ShowDialog('communaute');
        document.getElementById('communaute').querySelector('#message').innerHTML = objet.message;
    }

    pad(num: number, size: number): string {
        let s = num + '';
        while (s.length < size) { s = '0' + s; }
        return s;
    }


    LancerDesResultat(objet: any) {
        let premierDesFini = false;

        const resultatAnim = () => {
            const joueur = this.joueurs[objet.joueur];
            const pion = <GameObject>joueur.pion;

            const ecart = objet.nouvelleCase - joueur.case;
            joueur.case = objet.nouvelleCase;

            const chaineCase = 'Case' + this.pad(joueur.case, 3);
            const dummyCase = this.GetChildByName(chaineCase);

            const position = dummyCase.GetPosition().GetValue();

            const actuelle = pion.GetPosition().GetValue();

            pion.GetPosition().GetAnimationChannel().Clear();
            pion.GetPosition().GetAnimationChannel().AddTimingAndValue(0, [
                actuelle[0],
                actuelle[1],
                actuelle[2]
            ]);

            pion.GetPosition().GetAnimationChannel().AddTimingAndValue(0.5, [
                (actuelle[0] + position[0]) / 2,
                0.5,
                (actuelle[2] + position[2]) / 2
            ]);

            pion.GetPosition().GetAnimationChannel().AddTimingAndValue(1, [
                position[0], position[1], position[2]
            ]);
            pion.GetAnimation(0).SetEndEvent(() => {
                if (objet.joueur === this.numJoueur) {
                    this.SendToServer({ code: 'quefaire', joueur: this.numJoueur });
                }
                pion.GetAnimation(0).SetEndEvent(null);
            });
            pion.GetAnimation(0).SetEnd(1);
            pion.GetAnimation(0).Reset();
            pion.GetAnimation(0).Play();



        };

        this.des.SetVisible(true);
        const anim1 = this.des.SetCurrentAnimation(objet.des1);
        anim1.Reset();
        anim1.Play();
        anim1.SetEndEvent(() => {
            if (premierDesFini) {
                resultatAnim();
            }
            premierDesFini = true;
            anim1.SetEndEvent(null);
        });

        this.des2.SetVisible(true);
        const anim = this.des2.SetCurrentAnimation(objet.des2);
        anim.Reset();
        anim.Play();
        anim.SetEndEvent(() => {
            if (premierDesFini) {
                resultatAnim();
            }
            premierDesFini = true;
            anim.SetEndEvent(null);
        });

    }

    LancerDes() {
        document.getElementById('SortiePrison').style.display = 'none';
        document.getElementById('LancerDes').style.display = 'none';
        this.SendToServer({ code: 'lancerDes', joueur: this.numJoueur });
    }

    AQui(numJoueur: number) {
        document.getElementById('debutPartie').style.display = 'none';
        const cestmoi = (this.numJoueur === numJoueur);
        document.getElementById('LancerDes').style.display = (cestmoi && (this.joueurs[numJoueur].testPrison < 3)) ? 'block' : 'none';
        document.getElementById('SortiePrison').style.display = (cestmoi && (this.joueurs[numJoueur].prison)) ? 'block' : 'none';

        for (const joueur of this.joueurs) {
            if (joueur == null) { break; }

            // on éteint toutes les lumières
            (joueur.pion.GetChildByName('lumierePion') as Light).SetEnabled(false);

        }

        // on allume la lumière du joueur en cours.
        (this.joueurs[numJoueur].pion.GetChildByName('lumierePion') as Light).SetEnabled(true);
    }

    UpdateChat(objet: any) {
        const history = document.getElementById('history');
        const nouvelleDiv = document.createElement('div');
        nouvelleDiv.innerHTML = this.joueurs[objet.joueur].pseudo + ' :<br/>' + objet.content + '<hr/>';
        history.appendChild(nouvelleDiv);
    }

    ShowDialog(popupId: string) {
        document.getElementById('backdialog').style.display = 'flex';
        document.getElementById(popupId).style.display = 'flex';

    }

    SendToServer(object: any) {
        this.websocket.send(JSON.stringify(object));
    }

    Suivant() {
        this.SendToServer({ code: 'suivant' });
    }

    Pret() {
        this.SendToServer({ code: 'pret', numjoueur: this.numJoueur });
    }
    LancerPartie() {
        this.SendToServer({ code: 'demarrer' });
    }

    SortiePrison() {
        this.SendToServer({ code: 'sortirPrison', joueur: this.numJoueur });
    }

    AfficherPropriete(propriete: any) {

        if (document.getElementById('backdialog').style.display === 'none') {

            const contentCarte = document.getElementById('achatMaison');
            this.RemplirPropriete(propriete, contentCarte as HTMLDivElement);

            document.getElementById('acheterMaison').onclick = () => {
                this.SendToServer({ code: 'acheterMaison', joueur: this.numJoueur, propriete: propriete });
            };

            document.getElementById('vendreMaison').onclick = () => {
                this.SendToServer({ code: 'vendreMaison', joueur: this.numJoueur, propriete: propriete });
            };

            this.ShowDialog('achatMaison');
        }

    }

    UpdateJoueurs(joueurs: any, lescases: any) {
        for (let i = 0; i < joueurs.length; i++) {

            if (this.joueurs[i] == null) {
                this.joueurs[i] = joueurs[i];

                this.AddBillet(i + 1, 1, this.joueurs[i].billet1);
                this.AddBillet(i + 1, 5, this.joueurs[i].billet5);
                this.AddBillet(i + 1, 10, this.joueurs[i].billet10);
                this.AddBillet(i + 1, 20, this.joueurs[i].billet20);
                this.AddBillet(i + 1, 50, this.joueurs[i].billet50);
                this.AddBillet(i + 1, 100, this.joueurs[i].billet100);
                this.AddBillet(i + 1, 500, this.joueurs[i].billet500);
                this.AddBillet(i + 1, 1000, this.joueurs[i].billet1000);
            } else {
                this.joueurs[i].argent = joueurs[i].argent;
                this.joueurs[i].pret = joueurs[i].pret;
                this.joueurs[i].pseudo = joueurs[i].pseudo;
                this.joueurs[i].proprietes = joueurs[i].proprietes;
                this.joueurs[i].prison = joueurs[i].prison;
                this.joueurs[i].nbDouble = joueurs[i].nbDouble;
                this.joueurs[i].testPrison = joueurs[i].testPrison;





                let diff = joueurs[i].billet1 - this.joueurs[i].billet1;
                (diff > 0) ? this.AddBillet(i + 1, 1, diff) : this.RemoveBillet(i + 1, 1, -diff);
                diff = joueurs[i].billet5 - this.joueurs[i].billet5;
                (diff > 0) ? this.AddBillet(i + 1, 5, diff) : this.RemoveBillet(i + 1, 5, -diff);
                diff = joueurs[i].billet10 - this.joueurs[i].billet10;
                (diff > 0) ? this.AddBillet(i + 1, 10, diff) : this.RemoveBillet(i + 1, 10, -diff);
                diff = joueurs[i].billet20 - this.joueurs[i].billet20;
                (diff > 0) ? this.AddBillet(i + 1, 20, diff) : this.RemoveBillet(i + 1, 20, -diff);
                diff = joueurs[i].billet50 - this.joueurs[i].billet50;
                (diff > 0) ? this.AddBillet(i + 1, 50, diff) : this.RemoveBillet(i + 1, 50, -diff);
                diff = joueurs[i].billet100 - this.joueurs[i].billet100;
                (diff > 0) ? this.AddBillet(i + 1, 100, diff) : this.RemoveBillet(i + 1, 100, -diff);
                diff = joueurs[i].billet500 - this.joueurs[i].billet500;
                (diff > 0) ? this.AddBillet(i + 1, 500, diff) : this.RemoveBillet(i + 1, 500, -diff);
                diff = joueurs[i].billet1000 - this.joueurs[i].billet1000;
                (diff > 0) ? this.AddBillet(i + 1, 1000, diff) : this.RemoveBillet(i + 1, 1000, -diff);

                this.joueurs[i].billet1 = joueurs[i].billet1;
                this.joueurs[i].billet5 = joueurs[i].billet5;
                this.joueurs[i].billet10 = joueurs[i].billet10;
                this.joueurs[i].billet20 = joueurs[i].billet20;
                this.joueurs[i].billet50 = joueurs[i].billet50;
                this.joueurs[i].billet100 = joueurs[i].billet100;
                this.joueurs[i].billet500 = joueurs[i].billet500;
                this.joueurs[i].billet1000 = joueurs[i].billet1000;

                // on charge le model du pion du joueur
                if (this.joueurs[i].pion === undefined || !(this.joueurs[i].pion instanceof GameObject)) {
                    const loader = new GLTFLoader();
                    switch (joueurs[i].pion) {
                        case 1: {
                            this.joueurs[i].pion = loader.OpenFile('./src/objects/pion_out/pion.gltf');
                            break;
                        }
                        case 2: {
                            this.joueurs[i].pion = loader.OpenFile('./src/objects/voiture_out/voiture.gltf');
                            break;
                        }
                        case 3: {
                            this.joueurs[i].pion = loader.OpenFile('./src/objects/desACoudre_out/desACoudre.gltf');
                            break;
                        }
                        case 4: {
                            this.joueurs[i].pion = loader.OpenFile('./src/objects/chapeau_out/chapeau.gltf');
                            break;
                        }

                    }

                    const animation = new Animation();
                    const animatedChannel = new AnimationChannel();
                    animation.AddChannel(animatedChannel);

                    (this.joueurs[i].pion as GameObject).GetPosition().SetAnimation(animatedChannel);
                    (this.joueurs[i].pion as GameObject).AddAnimation(animation);

                    const caseD = <Entity>this.objet.GetChildByName('Case000');

                    animatedChannel.AddTimingAndValue(0, [caseD.GetPosition().GetValue()[0],
                    caseD.GetPosition().GetValue()[1],
                    caseD.GetPosition().GetValue()[2]]);

                    this.joueurs[i].pion.SetParent(this);

                    const lightPion = new Light(LIGHT_TYPE.SPOT);
                    lightPion.SetName('lumierePion');
                    lightPion.SetPosition(0, 0.7, 0);
                    lightPion.SetLightColor(2, 2, 2);
                    lightPion.SetEnabled(false);
                    lightPion.SetParent(this.joueurs[i].pion);

                }


                let j = 0;
                for (const lacase of lescases) {
                    if (lacase !== -1) {

                        if (this.casePropriete[j] === undefined) { this.casePropriete[j] = { joueur: -1, pion: null }; }

                        if (this.casePropriete[j].joueur !== lacase) {
                            if (this.casePropriete[j].joueur !== -1) {
                                this.casePropriete[j].pion.Destroy();
                            }

                            const appartient = this.joueurs[lacase].pion.CreateInstance() as GameObject;
                            const positionCase = this.GetChildByName('Case' + this.pad(j, 3));
                            appartient.GetPosition().SetAnimation(null);
                            appartient.GetScale().SetAnimation(null);

                            if (j > 0 && j < 10) {
                                appartient.SetPosition(0, 0.16, 0);
                            } else if (j > 10 && j < 20) {
                                appartient.SetPosition(0.16, 0, 0);
                            } else if (j > 20 && j < 30) {
                                appartient.SetPosition(0, -0.16, 0);
                            } else if (j > 30 && j < 40) {
                                appartient.SetPosition(-0.16, 0, 0);
                            }

                            appartient.SetRotation(1, 0, 0, 90);
                            appartient.SetScale(0.2, 0.2, 0.2);
                            appartient.SetParent(positionCase);



                            this.casePropriete[j].joueur = lacase;
                            this.casePropriete[j].pion = appartient;
                        }




                    }
                    j++;
                }

                // si c'est moi je met à jour les propriétés que je possède
                if (i === this.numJoueur) {
                    for (const prop of this.joueurs[i].proprietes) {

                        let groupe = this.groupeProprietes.groupe[prop.group];
                        if (groupe === undefined) {
                            const divGroupe = document.createElement('div');
                            divGroupe.style.position = 'relative';
                            divGroupe.style.width = '150px';
                            divGroupe.style.top = '-300px';
                            document.getElementById('proprietesPossedees').appendChild(divGroupe);


                            groupe = {
                                divGroupe: divGroupe,
                                proprietes: []
                            };
                            this.groupeProprietes.groupe[prop.group] = groupe;
                        }

                        let propriete = groupe.proprietes[prop.num];
                        if (propriete === undefined) {
                            propriete = prop;
                            const divCarte = document.createElement('div');
                            divCarte.style.backgroundColor = 'white';
                            divCarte.style.borderRadius = '10px';
                            divCarte.style.padding = '5px';
                            divCarte.style.boxShadow = '0 0 14px black';
                            divCarte.style.zIndex = prop.num;
                            divCarte.style.top = (prop.num * 50) + 'px';
                            divCarte.style.width = '100%';
                            divCarte.style.position = 'absolute';
                            divCarte.style.pointerEvents = 'auto';
                            groupe.proprietes[prop.num] = divCarte;
                            if (propriete.type === 'Propriete') {
                                divCarte.innerHTML = document.getElementById('achatPropriete').innerHTML;
                                divCarte.onclick = () => {
                                    this.AfficherPropriete(propriete);
                                };

                            } else if (propriete.type === 'Service') {
                                divCarte.innerHTML = document.getElementById('achatService').innerHTML;
                            } else if (propriete.type === 'Gare') {
                                divCarte.innerHTML = document.getElementById('achatGare').innerHTML;
                            }
                            groupe.divGroupe.appendChild(divCarte);


                            divCarte.querySelector('#boutons').innerHTML = '';
                            this.RemplirPropriete(propriete, divCarte as HTMLDivElement);
                            if (propriete.type === 'Propriete') {
                                (divCarte.querySelector('#achat').parentNode as HTMLDivElement).innerHTML = '';
                            }
                        }
                    }
                }
            }
        }
        this.UpdateInterface();
    }

    UpdateInterface() {

        const joueur = this.joueurs[this.numJoueur];
        const argent = joueur.argent;


        const listeJoueur = document.getElementById('listeJoueur');
        listeJoueur.innerHTML = '';
        let allpret = true;
        for (const player of this.joueurs) {
            if (player == null) { continue; }
            if (!player.pret) {
                allpret = false;
            }
        }

        for (const player of this.joueurs) {
            if (player == null) { continue; }

            const joueurDiv = document.createElement('div');
            joueurDiv.style.display = 'flex';
            joueurDiv.style.flexDirection = 'row';

            const pseudojoueur = document.createElement('div');
            pseudojoueur.innerHTML = player.pseudo;
            pseudojoueur.style.flex = '1';
            joueurDiv.appendChild(pseudojoueur);

            const etat = document.createElement('div');
            etat.style.transition = 'color 1s linear';
            etat.style.color = player.pret ? 'darkgreen' : 'darkred';
            etat.innerHTML = player.pret ? 'pret' : 'pas pret';


            joueurDiv.appendChild(etat);
            listeJoueur.appendChild(joueurDiv);
            if (player.pret) {
                etat.style.animation = 'change-color 1s cubic-bezier(0.2,1,0.25,1) 0s 1 normal';
            }

            if (allpret) {
                etat.innerHTML = player.argent + ' €';
            }

        }

        if (this.numJoueur === 0) {
            if (allpret) {
                document.getElementById('demarrerPartie').style.display = 'block';
            } else {
                document.getElementById('demarrerPartie').style.display = 'none';
            }
        }

    }

    ClearBillet(joueur: number, billet: number) {
        // tslint:disable-next-line:no-eval
        const elem = this.objet.GetChildByName('J' + joueur + '_B' + billet);
        elem.ClearChild();

    }

    RemoveBillet(joueur: number, billet: number, nb: number) {

        if (this.billetsJoueurs[joueur] === undefined) { this.billetsJoueurs[joueur] = new Array<any>(); }
        if (this.billetsJoueurs[joueur][billet] === undefined) { this.billetsJoueurs[joueur][billet] = new Array<any>(); }


        const array = this.billetsJoueurs[joueur][billet].splice(-nb, nb);
        for (const obj of array as Array<GameObject>) {
            obj.Destroy();
        }
    }

    AddBillet(joueur: number, billet: number, nb: number) {
        const elem = this.objet.GetChildByName('J' + joueur + '_B' + billet);
        elem.SetScale(1, 1, 1);
        // tslint:disable-next-line:no-eval
        const obj = <GameObject>eval('this.billet' + billet);

        if (this.billetsJoueurs[joueur] === undefined) { this.billetsJoueurs[joueur] = new Array<any>(); }
        if (this.billetsJoueurs[joueur][billet] === undefined) { this.billetsJoueurs[joueur][billet] = new Array<any>(); }

        const dejaPresent = this.billetsJoueurs[joueur][billet].length;

        for (let i = dejaPresent; i < (dejaPresent + nb); i++) {
            const nouveau = obj.CreateInstance();
            nouveau.SetVisible(true);
            nouveau.SetRotation(1, 0, 0, 90);
            nouveau.GetChild()[0].SetRotation(0, 1, 0, Math.random() * 8 - 4);
            nouveau.SetScale(1, 1, 1);
            const animate = new AnimationChannel();
            animate.SetTimingsAndValues([0, 0.5 + i * 0.02], [0, 0, 2, 0, 0, i * 0.005]);
            nouveau.GetPosition().SetAnimation(animate);
            const animation = new Animation();
            animation.SetEnd(0.5 + i * 0.02);
            animation.AddChannel(animate);
            nouveau.AddAnimation(animation);

            animation.Play();
            nouveau.SetParent(elem);


            this.billetsJoueurs[joueur][billet].push(nouveau);
        }

    }

    MouseWheel(evt: MouseWheelEvent) {
        this.radius += evt.wheelDeltaY / 150;
    }

    MouseMove(evt: MouseEvent) {
        super.MouseMove(evt);

        // tslint:disable-next-line:no-bitwise
        if (evt.buttons & 1) {
            if (this.precedentPosition[0] !== 0) {
                const ecartX = evt.screenX - this.precedentPosition[0];
                this.theta += ecartX / 300.0;
            }
            if (this.precedentPosition[1] !== 0) {
                const ecartX = evt.screenY - this.precedentPosition[1];
                this.phi -= ecartX / 300.0;
            }
        }
        this.precedentPosition[0] = evt.screenX;
        this.precedentPosition[1] = evt.screenY;

    }

    BeforeRendering(ellapsed: number) {
        if (!this.objet) { return; }

        this.exposure = 0.5;

        if (this.phi <= 0) {
            this.phi = 0.01;
        }
        if (this.phi > Math.PI / 2.0) {
            this.phi = Math.PI / 2.0;
        }

        if (this.radius < 1) {
            this.radius = 1;
        }
        if (this.radius > 6) {
            this.radius = 6;
        }

        const eyeX = this.radius * Math.sin(this.phi) * Math.cos(this.theta);
        const eyeY = this.radius * Math.cos(this.phi);
        const eyeZ = this.radius * Math.sin(this.phi) * Math.sin(this.theta);

        this.camera.SetPosition(eyeX, eyeY, eyeZ);
        this.camera.SetTargetView(0, 0, 0);

        super.BeforeRendering(ellapsed);
    }
}

(window as any).game = new Game();
