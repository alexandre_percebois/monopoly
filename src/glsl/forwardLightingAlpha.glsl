#version 300 es
            
precision highp float;
in vec3 glNormal;
in vec4 posWorld;
in vec2 uv;
in vec4 Vertex_EyeVec;

uniform vec4 color;
uniform int usenormalmap;
uniform sampler2D colormap;
uniform sampler2D normalmap;
uniform sampler2D roughnessmap;
uniform sampler2D emissivemap;
uniform sampler2D occlusionmap;
uniform sampler2D depth;
    
uniform mat4 viewe;
uniform mat4 lightMat;
uniform vec3 lightColor;
uniform vec4 lightInfo;

uniform sampler2D iblLut;
uniform samplerCube irradiances;
uniform samplerCube skybox;
const float PI = 3.14159265359;

out vec4 outcolor;

vec4 fresnelSchlick(float cosTheta, vec4 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}  

vec4 fresnelSchlickRoughness(float cosTheta, vec4 F0, float roughness)
{
    return F0 + (max(vec4(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}   

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
    
    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;
    
    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;
    
    return num / denom;
}
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);
    
    return ggx1 * ggx2;
}

void main(){
    float depthValue = texelFetch(depth, ivec2(gl_FragCoord.xy),0).r;
    float currentDepth = gl_FragCoord.z;
    if(currentDepth>depthValue) discard;

    vec4 mat = color * texture(colormap,uv);
    if(mat.a == 0.0) discard;
    
    vec4 emissive = texture(emissivemap,uv);
    vec3 normal = normalize(glNormal);
    vec3 normalfrommap = texture(normalmap,uv).xyz*2.0-1.0;
    vec4 roughnessColor = texture(roughnessmap,uv);
    float occlusion = texture(occlusionmap,uv).r;

    if(usenormalmap==1){
        vec3 normalizeEyeVec = normalize(Vertex_EyeVec.xyz);
        vec3 dp1 = dFdx( normalizeEyeVec );
        vec3 dp2 = dFdy( normalizeEyeVec );
        vec2 duv1 = dFdx( uv );
        vec2 duv2 = dFdy( uv );
        
        vec3 dp2perp = cross( dp2, normal );
        vec3 dp1perp = cross( normal, dp1 );
        vec3 T = normalize(dp2perp * duv1.x + dp1perp * duv2.x);
        vec3 B = normalize(dp2perp * duv1.y + dp1perp * duv2.y);
        
        float invmax = inversesqrt( max( dot(T,T), dot(B,B) ) );
        
        mat3 tbn=  mat3( T*invmax,B*invmax, normal );
        vec3 n = normalize(tbn*normalfrommap);
        normal = n;

    }   

    vec4 roughness = vec4(roughnessColor.g,roughnessColor.b,0,1);
    vec4 pos = lightMat * vec4(0,0,0,1);
    vec4 diffuse=vec4(0,0,0,0);
    vec4 specular = vec4(0,0,0,0);
    float NdotL=0.0;
    
    if(lightInfo.r==0.0){
        vec3 posworld = posWorld.xyz;
        vec3 viewDirection = normalize((viewe * vec4(0.0, 0.0, 0.0,1.0)).xyz-posworld);
        vec3 reflecte = reflect(-viewDirection,normal);
        vec4 reflectedColor = textureLod(skybox, reflecte,  roughness.r * 4.0);

        vec4 irra       = texture(irradiances,normal);
        

        vec3 N          = normal;
        vec3 V          = viewDirection;
        vec4 F0         = vec4(0.04); 
        F0              = mix(F0, mat, roughness.g);
        vec4 F          = fresnelSchlickRoughness(max(dot(N, V), 0.0), F0,roughness.r);
        vec4 kD         = 1.0 - F;
        kD              *= 1.0 - roughness.g;
        vec2 envBRDF    = texture(iblLut, vec2(max(dot(N, V), 0.0), roughness.r)).rg;

        diffuse = mat*kD*irra;
        specular = reflectedColor * (F*envBRDF.x+envBRDF.y); 
    }
    else {
        vec3 posworld = posWorld.xyz;
        vec3 lightDirection = normalize(pos.xyz-posworld);
        vec3 viewDirection = normalize((viewe * vec4(0.0, 0.0, 0.0,1.0)).xyz-posworld);
        
        vec3 N = normal;
        vec3 V = viewDirection;

        vec3 L = normalize(pos.xyz - posworld);
        if(lightInfo.r==1.0) L = normalize(pos.xyz);
        
        vec3 H = normalize(V + L);

        float distance    = length(pos.xyz-posworld);
        float attenuation = 1.0 / (distance * distance);
        if(lightInfo.r==1.0) attenuation = 1.0;
        vec4 radiance     = vec4(lightColor,1) * attenuation; 

        vec4 F0 = vec4(0.04); 
        F0      = mix(F0, mat, roughness.g);
        vec4 F  = fresnelSchlickRoughness(max(dot(H, V), 0.0), F0, roughness.r);

        float NDF = DistributionGGX(N, H, roughness.r);       
        float G   = GeometrySmith(N, V, L, roughness.r);       

        vec4 numerator    = NDF * G * F;
        float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
        vec4 spec     = numerator / max(denominator, 0.001);  

        vec4 kS = F;
        vec4 kD = vec4(1.0) - kS;
        kD *= 1.0 - roughness.g;	
    
        float spotAttenuation = 1.0; 
        NdotL = max(dot(N, L), 0.0);        
        if(lightInfo.r==3.0){
            vec3 spotDirection = normalize((lightMat * vec4(0, -1, 0, 1) - pos).xyz); 
            float spotEffect = acos(dot(-lightDirection, spotDirection)); 
            if (spotEffect < lightInfo.w) {
                if (NdotL > 0.0) {
                    spotAttenuation = 1.0 - smoothstep(lightInfo.z, lightInfo.w, spotEffect); 
                } 
            } else { spotAttenuation = 0.0; } 
                
        }
        diffuse = (kD * mat) * radiance * NdotL * spotAttenuation;
        specular = spec * radiance * NdotL * spotAttenuation;

        
    } 
    outcolor = (diffuse + specular)* occlusion * mat.a ;
}