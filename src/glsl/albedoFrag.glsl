#version 300 es
        
precision highp float;
in vec3 glNormal;
in vec4 posWorld;
in vec2 uv;
in vec4 Vertex_EyeVec;

uniform int usenormalmap;

uniform vec4 color;
uniform float alphaCutOff;
uniform float alphaMode;

uniform sampler2D colormap;
uniform sampler2D normalmap;
uniform sampler2D roughnessmap;
uniform sampler2D emissivemap;
uniform sampler2D occlusionmap;

out vec4 outColor[4];

void main() {
    if(alphaMode != 0.0)
    if(texture(colormap,uv).a < alphaCutOff) discard;
    
    vec3 normal = normalize(glNormal);

    vec3 normalfrommap = texture(normalmap,uv).xyz*2.0-1.0;
    vec4 roughnessColor = texture(roughnessmap,uv);
    float occlusion = texture(occlusionmap,uv).r;


    if(usenormalmap==1){
        vec3 normalizeEyeVec = normalize(Vertex_EyeVec.xyz);
        vec3 dp1 = dFdx( normalizeEyeVec );
        vec3 dp2 = dFdy( normalizeEyeVec );
        vec2 duv1 = dFdx( uv );
        vec2 duv2 = dFdy( uv );
        
        vec3 dp2perp = cross( dp2, normal );
        vec3 dp1perp = cross( normal, dp1 );
        vec3 T = normalize(dp2perp * duv1.x + dp1perp * duv2.x);
        vec3 B = normalize(dp2perp * duv1.y + dp1perp * duv2.y);
        
        float invmax = inversesqrt( max( dot(T,T), dot(B,B) ) );
        
        mat3 tbn=  mat3( T*invmax,B*invmax, normal );
        vec3 n = normalize(tbn*normalfrommap);
        
        outColor[0] = vec4(n ,1);
    }
    else
    {
        outColor[0] = vec4(normal,1);
    }
    outColor[1] = vec4(color.rgb * texture(colormap,uv).rgb , occlusion);
    outColor[2] = vec4(roughnessColor.g,roughnessColor.b,0,1);
    outColor[3] = texture(emissivemap,uv);
}