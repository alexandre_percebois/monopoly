#version 300 es
precision highp float;
in vec2 v_texcoord;

uniform mat4 perspective;
uniform mat4 view;
uniform mat4 lightMat;
uniform vec3 lightColor;
uniform vec4 lightInfo;

uniform sampler2D normal;        
uniform sampler2D material;
uniform sampler2D roughMetal;
uniform sampler2D depth;
uniform sampler2D iblLut;
uniform samplerCube irradiances;
uniform samplerCube skybox;

const float PI = 3.14159265359;

out vec4 color;
vec3 WorldPosFromDepth(float depth) {
    float z = depth * 2.0 - 1.0;
    vec4 clipSpacePosition = vec4(v_texcoord * 2.0 - 1.0, z, 1.0);
    vec4 viewSpacePosition = perspective * clipSpacePosition;
    viewSpacePosition /= viewSpacePosition.w;
    vec4 worldSpacePosition = view * viewSpacePosition;
    return worldSpacePosition.xyz;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}  

vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}   

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
    
    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;
    
    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;
    
    return num / denom;
}
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);
    
    return ggx1 * ggx2;
}

void main(){
    vec4 mat = texture(material,v_texcoord);
    vec3 normal = normalize(texture(normal,v_texcoord).xyz);
    vec4 roughness = texture(roughMetal,v_texcoord);
    vec4 pos = lightMat * vec4(0,0,0,1);
    float depthColor = texture(depth,v_texcoord).r;
    vec4 diffuse=vec4(0,0,0,0);
    vec4 specular = vec4(0,0,0,0);
    float NdotL=0.0;
    if(depthColor == 1.0) discard;
    if(lightInfo.r==0.0){
        vec3 posworld = WorldPosFromDepth(depthColor);
        vec3 viewDirection = normalize((view * vec4(0.0, 0.0, 0.0,1.0)).xyz-posworld);
        vec3 reflecte = reflect(-viewDirection,normal);
        vec4 reflectedColor = textureLod(skybox, reflecte, roughness.r * 4.0);

        vec4 irra       = texture(irradiances,normal);
        

        vec3 N          = normal;
        vec3 V          = viewDirection;
        vec3 F0         = vec3(0.04); 
        F0              = mix(F0, mat.rgb, roughness.g);
        vec3 F          = fresnelSchlickRoughness(max(dot(N, V), 0.0), F0,roughness.r);
        vec3 kD         = 1.0 - F;
        kD              *= 1.0 - roughness.g;
        vec2 envBRDF    = texture(iblLut, vec2(max(dot(N, V), 0.0), roughness.r)).rg;

        diffuse = vec4(mat.rgb*kD*irra.rgb,1);
        specular = vec4(reflectedColor.rgb * F ,1); //  (F * envBRDF.x + envBRDF.y) ,1);
        
    }
    else {
        vec3 posworld = WorldPosFromDepth(depthColor);
        vec3 lightDirection = normalize(pos.xyz-posworld);
        vec3 viewDirection = normalize((view * vec4(0.0, 0.0, 0.0,1.0)).xyz-posworld);
        
        vec3 N = normal;
        vec3 V = viewDirection;

        vec3 L = normalize(pos.xyz - posworld);
        if(lightInfo.r==1.0) L = normalize(pos.xyz);
        
        vec3 H = normalize(V + L);

        float distance    = length(pos.xyz-posworld);
        //float attenuation = 1.0 / (distance * distance);
        float attenuation = clamp(1.0 -distance*distance/(5.0*5.0),0.0,1.0);
        attenuation*=attenuation;
        if(lightInfo.r==1.0) attenuation = 1.0;
        vec3 radiance     = lightColor * attenuation; 

        vec3 F0 = vec3(0.04); 
        F0      = mix(F0, mat.rgb, roughness.g);
        vec3 F  = fresnelSchlickRoughness(max(dot(H, V), 0.0), F0,roughness.r);

        float NDF = DistributionGGX(N, H, roughness.r);       
        float G   = GeometrySmith(N, V, L, roughness.r);       

        vec3 numerator    = NDF * G * F;
        float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
        vec3 spec     = numerator / max(denominator, 0.001);  

        vec3 kS = F;
        vec3 kD = vec3(1.0) - F;
        kD *= 1.0 - roughness.g;	
    
        float spotAttenuation = 1.0; 
        NdotL = max(dot(N, L), 0.0);        
        if(lightInfo.r==3.0){
            vec3 spotDirection = normalize((lightMat * vec4(0, -1, 0, 1) - pos).xyz); 
            float spotEffect = acos(dot(-lightDirection, spotDirection)); 
            if (spotEffect < lightInfo.w) {
                if (NdotL > 0.0) {
                    spotAttenuation = 1.0 - smoothstep(lightInfo.z, lightInfo.w, spotEffect); 
                } 
            } else { spotAttenuation = 0.0; } 
                
        }
        diffuse = vec4(kD * mat.rgb * radiance * NdotL * spotAttenuation,1);
        specular = vec4(spec * radiance * NdotL * spotAttenuation,1);
    } 

    color = vec4((diffuse.rgb + specular.rgb)* mat.a,1); 
}