#version 300 es

precision highp float;

in vec2 v_texcoord;

out vec4 FragColor;

uniform float blur;
uniform mat4 view;
uniform mat4 perspective;

uniform samplerCube skybox;

vec3 WorldPosFromDepth(float depth);

void main(){

    vec3 wp = WorldPosFromDepth(1.0);
    vec3 campos = (view*vec4(0,0,0,1)).xyz;

    FragColor = textureLod(skybox, wp-campos,blur);

}

vec3 WorldPosFromDepth(float depth) {
    float z = depth * 2.0 - 1.0;
    vec4 clipSpacePosition = vec4(v_texcoord * 2.0 - 1.0, z, 1.0);
    vec4 viewSpacePosition = perspective * clipSpacePosition;
    viewSpacePosition /= viewSpacePosition.w;
    vec4 worldSpacePosition = view * viewSpacePosition;
    return worldSpacePosition.xyz;
}
