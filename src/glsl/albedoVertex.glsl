#version 300 es
        
in vec3 a_position;
in vec3 a_normal;
in vec2 a_texcoord0;

in mat4 model;

uniform mat4 view;
uniform mat4 perspective;
out vec3 glNormal;
out vec4 posWorld;
out vec2 uv;
out vec4 Vertex_EyeVec;
void main(){
    gl_Position = (perspective*view*model)*vec4(a_position,1);
    glNormal = mat3(model)*a_normal; 
    posWorld = model * vec4(a_position,1);
    uv = a_texcoord0;
    Vertex_EyeVec = view * model * vec4(a_position,1);
}