#version 300 es
precision highp float;
const float gamma = 2.2;
  
in vec2 v_texcoord;

uniform float exposure;

uniform sampler2D hdr;
uniform sampler2D bloom;

out vec4 FragColor;

void main(){

    vec4 hdrColor = texture(hdr, v_texcoord);      
    vec4 bloomColor = texture(bloom, v_texcoord);
    hdrColor += bloomColor; // additive blending
    // tone mapping
    
    vec3 result = vec3(1.0) - exp(-hdrColor.rgb * exposure);
    // also gamma correct while we're at it       
    // result = pow(result, vec3(1.0 / gamma));
    FragColor = vec4(result,hdrColor.a);
    // FragColor = hdrColor;
    

}