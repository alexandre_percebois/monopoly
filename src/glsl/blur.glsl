#version 300 es

precision highp float;

out vec4 FragColor;
  
in vec2 v_texcoord;

uniform sampler2D image;
  
uniform bool horizontal;
uniform float weight[5]; //float[] (0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216);

void main()
{             
    vec2 tex_offset = vec2(1.0) / vec2(textureSize(image, 0)); // gets size of single texel
    vec4 result = texture(image, v_texcoord) * weight[0]; // current fragment's contribution
    if(horizontal)
    {
        for(float i = 1.0; i < 5.0; ++i)
        {
            result += texture(image, v_texcoord + vec2(tex_offset.x * i, 0.0)) * weight[int(i)];
            result += texture(image, v_texcoord - vec2(tex_offset.x * i, 0.0)) * weight[int(i)];
        }
    }
    else
    {
        for(float i = 1.0; i < 5.0; ++i)
        {
            result += texture(image, v_texcoord + vec2(0.0, tex_offset.y * i)) * weight[int(i)];
            result += texture(image, v_texcoord - vec2(0.0, tex_offset.y * i)) * weight[int(i)];
        }
    }
    FragColor = result;
}