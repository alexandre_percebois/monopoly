#version 300 es
in vec3 coordinates; 
in vec2 a_texcoord;

out vec2 v_texcoord;

void main(void) { 
    gl_Position = vec4(coordinates, 1.0); 
    v_texcoord = a_texcoord;
}