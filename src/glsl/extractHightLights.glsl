#version 300 es

precision highp float;

in vec2 v_texcoord;

out vec4 FragColor;

uniform float exposure;
uniform sampler2D image;
const float gamma = 2.2;

void main(){

    vec3 hdrColor = texture(image, v_texcoord).rgb;

    hdrColor = vec3(1.0)-exp(-hdrColor*exposure);

    float brightness = dot(hdrColor, vec3(0.2126, 0.7152, 0.0722));

    if(brightness > 1.0){
        FragColor = vec4(hdrColor,1);
    }
    else
        FragColor  = vec4(0,0,0,0);
}